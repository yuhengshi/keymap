import keyboard
import json
import logging

logging.basicConfig(level=logging.INFO)


def map_key(conf_path):
    try:
        logging.info("Please keep map.json and your .exe in the same path")
        logging.info(f"key map path: {conf_path}")
        with open(conf_path, "r", encoding="utf-8") as f:
            json_content = f.read()
        key_map = json.loads(json_content)
        logging.info(key_map)
        for k, v in key_map.items():
            # 组合键
            if "+" in k + v:
                keyboard.remap_hotkey(k, v)
            else:
                keyboard.remap_key(k, v)
        logging.info("map success")
    except Exception as e:
        logging.error(str(e))
    keyboard.wait()


if __name__ == '__main__':
    conf = r"./map.json"
    map_key(conf)
